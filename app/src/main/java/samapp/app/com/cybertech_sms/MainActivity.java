package samapp.app.com.cybertech_sms;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.SmsManager;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import es.dmoral.toasty.Toasty;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import samapp.app.com.cybertech_sms.Utility.comman;

public class MainActivity extends AppCompatActivity {
    private Button pbtn;
    OkHttpClient client = new OkHttpClient();
    private ArrayList<String> sentmsges;
    private int dialog, mobitel;
    private AtomicBoolean finish = new AtomicBoolean(true);
    private Task tk;
    private AVLoadingIndicatorView avi;
    public static final int PERMISSION_ALL = 7;
    String[] PERMISSIONS = {
            Manifest.permission.SEND_SMS,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.INTERNET,
            Manifest.permission.ACCESS_NETWORK_STATE,
            Manifest.permission.ACCESS_WIFI_STATE
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pbtn = findViewById(R.id.btnprgress);

        avi = findViewById(R.id.avi);


        avi.hide();
        pbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!hasPermissions(MainActivity.this, PERMISSIONS)) {
                    ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS, PERMISSION_ALL);
                } else {
                    SubscriptionManager subscriptionManager = SubscriptionManager.from(MainActivity.this);
                    if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MainActivity.this, PERMISSIONS, PERMISSION_ALL);
                    } else {
                        List<SubscriptionInfo> subscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();
                        for (SubscriptionInfo subscriptionInfo : subscriptionInfoList) {
                            if (subscriptionInfo.getCarrierName().equals("Mobitel")) {
                                mobitel = subscriptionInfo.getSubscriptionId();
                            } else {
                                dialog = subscriptionInfo.getSubscriptionId();
                            }

                        }
                        if (comman.isNetworkAvailable(MainActivity.this)) {
                            //   pbtn.setEnabled(false);


                            if (finish.get()) {
                                finish.set(false);
                                pbtn.setText("Stop");
                                avi.show();
                                new Task(MainActivity.this).run();

                            } else {
                                avi.hide();
                                pbtn.setText("Start");
                                finish.set(true);
                            }


                        } else {
                            createNetErrorDialog();
                        }
                    }


                }


            }
        });
    }


    class Task implements Runnable {


        Context ct;


        public Task(Context ct) {

            this.ct = ct;
        }

        @Override
        public void run() {
            try {
                while (finish.get()) {


                    getnumbers();
                    Thread.sleep(10000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    private void getnumbers() {
        String url = "http://ecomm.tourislandsrilanka.com/api/sms";

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(url)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();

//        Response response = client.newCall(request).execute();
//        Log.e(TAG, response.body().string());

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                //Log.w("failure Response", mMessage);
                Toast.makeText(getApplicationContext(), mMessage, Toast.LENGTH_SHORT).show();
                //call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String mMessage = response.body().string();
                if (response.code() == 200 || response.code() == 201) {
                    try {
                        new sendsms(new JSONArray(mMessage)).execute();
                    } catch (JSONException e) {
                        MainActivity.this.runOnUiThread(new Runnable() {
                            public void run() {

                                //  pbtn.setEnabled(true);
                                Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                            }
                        });// hiveProgressView.setVisibility(View.GONE);
                        //Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_SHORT).show();
                    }
                } else {

                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            // pbtn.setEnabled(true);

                            Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                        }
                    });

                }


            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {

            case PERMISSION_ALL:

                if (grantResults.length > 0) {

                    if (hasPermissions(MainActivity.this, PERMISSIONS)) { // new Login(username.getText().toString(), password.getText().toString(), getApplicationContext()).execute();

                        Toasty.info(MainActivity.this, "Permission Granted", Toast.LENGTH_SHORT, true).show();
                    } else {

                        Toasty.error(MainActivity.this, "Permission Denied", Toast.LENGTH_SHORT, true).show();
                    }
                }

                break;
        }
    }

    private class sendsms extends AsyncTask<String, String, String> {
        private JSONArray smsList;

        public sendsms(JSONArray smsList) {
            this.smsList = smsList;
        }


        @Override
        protected String doInBackground(String... strings) {
            for (int i = 0; i < smsList.length(); i++) {
                try {
                    JSONObject ob = smsList.getJSONObject(i);
                    String firstnum = ob.getString("mobile").substring(0, 3);

                    if (firstnum.equals("077") || firstnum.equals("076")) {
                        SmsManager.getSmsManagerForSubscriptionId(dialog).sendTextMessage(ob.getString("mobile"), null, ob.getString("msg"), null, null);

                        updateRequest(ob.getString("id"));
                    } else {
                        SmsManager.getSmsManagerForSubscriptionId(mobitel).sendTextMessage(ob.getString("mobile"), null, ob.getString("msg"), null, null);


                        updateRequest(ob.getString("id"));
                    }


                } catch (Exception e) {
                    Log.e("error", e.toString());
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            //  pbtn.setEnabled(true);

        }
    }


    private void updateRequest(String id) throws IOException {

        MediaType MEDIA_TYPE = MediaType.parse("application/json");
        String url = "http://ecomm.tourislandsrilanka.com/api/updateSMS";

        OkHttpClient client = new OkHttpClient();

        JSONObject postdata = new JSONObject();
        try {
            postdata.put("id", id);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        RequestBody body = RequestBody.create(MEDIA_TYPE, postdata.toString());

        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                String mMessage = e.getMessage().toString();
                Log.w("failure Response", mMessage);
                //call.cancel();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                String mMessage = response.body().string();
//                Log.e(TAG, mMessage);
            }
        });
    }


    public static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    protected void createNetErrorDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Please turn on your network connection to continue.")
                .setTitle("No internet connection")
                .setCancelable(false)
                .setPositiveButton("Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Intent i = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                                startActivity(i);
                            }
                        }
                )
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                MainActivity.this.finish();
                            }
                        }
                );
        AlertDialog alert = builder.create();
        alert.show();
    }

}
